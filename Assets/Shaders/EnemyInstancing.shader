﻿
Shader "Unlit/CustomTextureArrayBlit"
{
       Properties
       {
        _MainTex ("Texture", any) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
       }
       SubShader
       {
              Tags { "RenderType"="Opaque" }
              Cull Off
              ZWrite Off
              ZTest Always
              Pass
              {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #define TRANSFORM_TEX(tex,name) (tex.xy * name##_ST.xy + name##_ST.zw)
                     
					 UNITY_DECLARE_TEX2DARRAY(_MainTex);
					uint _DepthSlice;
 
                     #include "UnityCG.cginc"
                     struct appdata
                     {
                           float4 vertex : POSITION;
                                           UNITY_VERTEX_INPUT_INSTANCE_ID
						   float2 uv : TEXCOORD0;
                     };
 
                     struct v2f
                     {
                           float4 vertex : SV_POSITION;
						   float3 uv : TEXCOORD0;
						                   UNITY_VERTEX_INPUT_INSTANCE_ID
                     };
 
             UNITY_INSTANCING_BUFFER_START(Props)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)            
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _MainTex_ST)
                UNITY_DEFINE_INSTANCED_PROP(float, _TextureIndex)
            UNITY_INSTANCING_BUFFER_END(Props)
                     v2f vert (appdata v)
                     {
                           v2f o;
                           
                            
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
 
                           o.vertex = mul(UNITY_MATRIX_P, v.vertex);
						   o.uv = float3(v.uv.xy, _DepthSlice);
 
                           return o;
                     }
 
                     fixed4 frag (v2f i) : SV_TARGET
                     {
                        UNITY_SETUP_INSTANCE_ID(i);
						 fixed4 color = UNITY_SAMPLE_TEX2DARRAY(_MainTex, i.uv) * UNITY_ACCESS_INSTANCED_PROP(Props, _Color);;
						return color;
                     }
                     ENDCG
              }
       }
}
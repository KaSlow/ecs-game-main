﻿using System;
using UnityEngine;

public class MonoInstancingTest : MonoBehaviour
{
	#region SerializeFields

	[SerializeField] private GameObject enemyPrefab;
	[SerializeField] private Material material;
	[SerializeField] private EnemyInstancingSettings instancing;
	
	private static readonly int Textures = Shader.PropertyToID("_Textures");
	private float m_maxTextureIndex;
	private float m_currentIndex;
	
	#endregion

	#region UnityMethods

	private void SetTextureArray()
	{
		// Texture2D[] textures = instancing..ToArr
		// m_maxTextureIndex = textures.Length;
		// m_currentIndex = 0;
		// int textureWidth = 2048;
		// int textureHeight = 2048;
		//
		// var textureArray = new Texture2DArray(textureWidth, textureHeight, textures.Length, TextureFormat.RGBA32, false);
		//
		// for (int i = 0; i < textures.Length; i++)
		// {
		// 	Graphics.CopyTexture(textures[i], 0, 0, textureArray, i, 0);
		// }
		//
		// material.SetTexture(Textures, textureArray);
	}

	private void SetTextureIndex(float textureIndex)
	{
		material.SetFloat("_TextureIndex", textureIndex);
	}

	private void Start()
	{
		SetTextureArray();
	}

	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.Plus))
		{
			SetTextureIndex(Mathf.Clamp(m_currentIndex++, 0, m_maxTextureIndex));
		}
		else if (Input.GetKeyUp(KeyCode.Minus))
		{
			SetTextureIndex(Mathf.Clamp(m_currentIndex--, 0, m_maxTextureIndex));
		}
	}

	#endregion
}

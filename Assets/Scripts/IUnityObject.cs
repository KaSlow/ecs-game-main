﻿using UnityEngine;

public interface IUnityObject
{
	#region PublicFields

	// ReSharper disable once InconsistentNaming
	Behaviour behaviour { get; }

	// ReSharper disable once InconsistentNaming
	GameObject gameObject { get; }

	// ReSharper disable once InconsistentNaming
	Transform transform { get; }

	#endregion
}
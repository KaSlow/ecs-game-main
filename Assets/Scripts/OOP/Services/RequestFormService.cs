using System.Collections;
using OOP.Forms;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace OOP.Services
{
    public static class RequestFormService
    {
        public static IEnumerator SendForm(
            RequestForm requestForm,
            Text panelTitle,
            Text panelContent,
            Image panelImage,
            BaseForm baseForm = null,
            bool authorization = false
        )
        {
            var request = new UnityWebRequest(requestForm.MUrl, requestForm.MMethod);
            if (requestForm.MJson != null)
            {
                byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(requestForm.MJson);
                request.uploadHandler = new UploadHandlerRaw(jsonToSend);
            }

            request.downloadHandler = new DownloadHandlerBuffer();

            // Sets headers to request
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");

            // For logged in users is needed to send Bearer token
            if (authorization)
            {
                request.SetRequestHeader("Authorization", requestForm.MToken);
            }

            yield return request.SendWebRequest();

            if (baseForm != null)
            {
                baseForm.TooglePanel();
                PanelFormService.AdaptPanelToJsonResponse(baseForm, request, panelTitle, panelContent, panelImage);
            }
        }
    }
}
using OOP.Config;
using OOP.Forms;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace OOP.Services
{
    public static class PanelFormService
    {
        public static void AdaptPanelToJsonResponse(
            BaseForm baseForm,
            UnityWebRequest unityWebRequest,
            Text panelTitle,
            Text panelContent,
            Image backgroundImage
        )
        {
            ApplyText(baseForm, unityWebRequest, panelTitle, panelContent);
            ApplyBackground(unityWebRequest, backgroundImage);
        }

        public static void ApplyBackground(UnityWebRequest unityWebRequest, Image backgroundImage)
        {
            long responseCode = unityWebRequest.responseCode;
            if (responseCode == 200 || responseCode == 201)
            {
                backgroundImage.color = Color.green;
            }
            else
            {
                backgroundImage.color = Color.red;
            }
        }


        public static void ApplyText(
            BaseForm baseForm,
            UnityWebRequest unityWebRequest,
            Text panelTitle,
            Text panelContent
        )
        {
            long responseCode = unityWebRequest.responseCode;
            if (responseCode == 200 || responseCode == 201 || responseCode == 204)
            {
                SetSuccessMessageToPanelFromJson(baseForm, unityWebRequest.downloadHandler.text, panelTitle);
            }
            else
            {
                SetErrorMessagesToPanelFromJson(unityWebRequest.downloadHandler.text, panelTitle, panelContent);
            }
        }

        private static void SetErrorMessagesToPanelFromJson(
            string json,
            Text panelTitle,
            Text panelContent
        )
        {
            panelTitle.text = "Errors";
            string contentText = ConvertJsonStringToText(json);
            panelContent.text = contentText;
        }

        private static void SetSuccessMessageToPanelFromJson(
            BaseForm baseForm,
            string json,
            Text panelTitle
        )
        {
            panelTitle.text = "Success";
            var jsonObject = JSON.Parse(json);
            baseForm.FillSuccessMessageWithContent(jsonObject);

            LoginForm loginForm = new LoginForm();
            // We have to save token in our settings
            // it will be used to each request after logged in
            if (loginForm.GetClassName() == baseForm.GetClassName())
            {
                PlayerPrefsManager.SetToken(jsonObject["token"]);
            }
        }

        public static string ConvertJsonStringToText(string json)
        {
            var jsonObject = JSON.Parse(json);
            string contentText = "";

            if (jsonObject.HasKey("errors"))
            {
                var jsonObjectErrors = jsonObject["errors"];
                foreach (string fieldName in GetErrorFieldNames())
                {
                    contentText = AppendValueOfJsonKeyToContentString(jsonObjectErrors, fieldName, contentText);
                }
            }
            else if (jsonObject.HasKey("message"))
            {
                contentText = AppendValueOfJsonKeyToContentString(jsonObject, "message", contentText);
            }
            else
            {
                contentText =
                    "Server response with incorrect data. If problem will repeat, please contact with support.";
            }

            return contentText;
        }

        public static string AppendValueOfJsonKeyToContentString(JSONNode simpleJson, string key, string content)
        {
            if (simpleJson.HasKey(key))
            {
                if (simpleJson[key].IsArray)
                {
                    for (int i = 0; i < simpleJson[key].Count; i++)
                    {
                        content += $"-   {simpleJson[key][i]} \n";
                    }
                }
                else
                {
                    content += $"-   {simpleJson[key]} \n";
                }
            }

            return content;
        }

        private static string[] GetErrorFieldNames()
        {
            return new[] {"username", "email", "password", "confirm_password", "token", "name"};
        }
    }
}
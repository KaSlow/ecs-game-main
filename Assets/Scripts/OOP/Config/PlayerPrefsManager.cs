using System.IO;
using Connections.Configs;
using UnityEngine;

namespace OOP.Config
{
    public class PlayerPrefsManager : MonoBehaviour
    {
        // Const to use it static in every place in game
        private string m_token = "token";
        private string m_characterId = "";

        // All functions are static to use it globally
        public static void SetToken(string token)
        {
            PlayerPrefsManager playerPrefsManager = new PlayerPrefsManager();
            if (Api.Host == "localhost:8000")
            {
                File.WriteAllText(GetPathToToken(), token);
            }
            else
            {
                PlayerPrefs.SetString(playerPrefsManager.m_token, token);
            }
        }

        public static string GetToken()
        {
            PlayerPrefsManager playerPrefsManager = new PlayerPrefsManager();
            if (Api.Host == "localhost:8000")
            {
                if (File.Exists(GetPathToToken()))
                {
                    return File.ReadAllText(GetPathToToken());
                }
            }
            return PlayerPrefs.GetString(playerPrefsManager.m_token);
        }

        public static void SetCharacterId(string characterId)
        {
            PlayerPrefsManager playerPrefsManager = new PlayerPrefsManager();
            if (Api.Host == "localhost:8000")
            {
                File.WriteAllText(GetPathToCharacterId(), characterId);
            }
            else
            {
                PlayerPrefs.SetString(playerPrefsManager.m_characterId, characterId);
            }
        }

        public static string GetCharacterId()
        {
            PlayerPrefsManager playerPrefsManager = new PlayerPrefsManager();
            if (Api.Host == "localhost:8000")
            {
                if (File.Exists(GetPathToToken()))
                {
                    return File.ReadAllText(GetPathToCharacterId());
                }
            }
            return PlayerPrefs.GetString(playerPrefsManager.m_characterId);
        }
        
        public static string GetPathToToken()
        {
            return Application.persistentDataPath + @"\token.txt";
        } 
        
        public static string GetPathToCharacterId()
        {
            return Application.persistentDataPath + @"\character_id.txt";
        }
    }
}
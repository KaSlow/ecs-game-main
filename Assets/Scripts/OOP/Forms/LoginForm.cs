using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Connections;
using SimpleJSON;

namespace OOP.Forms
{
    public class LoginForm : BaseForm
    {
        [SerializeField]
        public InputField UsernameField;
        public InputField PasswordField;

        public override void ClickSendRequestButton()
        {
            JsonObjectInString = new JSONObject(delegate(JSONObject request)
                {
                    request.AddField("username", UsernameField.text);
                    request.AddField("password", PasswordField.text);
                }
            ).ToString();
            SendRequest(Endpoint.GetFullEndpoint(Endpoint.Login), "POST", this);
        }

        public override void FillSuccessMessageWithContent(JSONNode jsonNode)
        {
            string contentText = "You have successfully logged.";
            PanelContent.text = contentText;
            IsSuccessResponse200 = true;
        }

        public override Type GetClassName()
        {
            return  MethodBase.GetCurrentMethod().DeclaringType;
        }
    }
}
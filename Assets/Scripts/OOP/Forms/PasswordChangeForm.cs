using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Connections;
using SimpleJSON;

namespace OOP.Forms
{
    public class PasswordChangeForm : BaseForm
    {
        [SerializeField]
        public InputField EmailField;
        public InputField PasswordField;
        public InputField ConfirmationPasswordField;
        public InputField TokenField;

        public override void ClickSendRequestButton()
        {
            JsonObjectInString = new JSONObject(delegate(JSONObject request)
                {
                    request.AddField("email", EmailField.text);
                    request.AddField("password", PasswordField.text);
                    request.AddField("confirm_password", ConfirmationPasswordField.text);
                    request.AddField("token", TokenField.text);
                }
            ).ToString();
            SendRequest(Endpoint.GetFullEndpoint(Endpoint.PasswordChange), "POST", this);
        }

        public override void FillSuccessMessageWithContent(JSONNode jsonNode)
        {
            string contentText = "You have successfully change Your password. " +
                                 "You can login to Your account now.";
            PanelContent.text = contentText;
            IsSuccessResponse200 = true;
        }
        public override Type GetClassName()
        {
            return  MethodBase.GetCurrentMethod().DeclaringType;
        }
    }
}
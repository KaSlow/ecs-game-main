using System;
using OOP.Services;
using SimpleJSON;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

namespace OOP.Forms
{
    public abstract class BaseForm : MonoBehaviour
    {
        protected string JsonObjectInString;
        protected bool PanelIsActive;
        protected bool IsSuccessResponse200 = false;

        [SerializeField]
        public GameObject Panel;
        public Text PanelTitle;
        public Text PanelContent;
        public Image PanelImage;

        private void Start()
        {
            PanelIsActive = true;
            TooglePanel();
        }

        public void SendRequest(string url, string method, BaseForm baseForm, bool authorization = false)
        {
            RequestForm requestForm = new RequestForm(url, JsonObjectInString, method);
            StartCoroutine(
                RequestFormService.SendForm(
                    requestForm,
                    PanelTitle,
                    PanelContent,
                    PanelImage,
                    baseForm,
                    authorization
                )
            );
        }

        public void TooglePanel()
        {
            Panel.SetActive(!PanelIsActive);
            PanelIsActive = !PanelIsActive;
        }

        public abstract void ClickSendRequestButton();

        public void ClickPanelButton(string scene)
        {
            if (IsSuccessResponse200)
            {
                SceneManager.LoadScene(scene);
            }
            else
            {
                TooglePanel();
            }
        }

        public abstract void FillSuccessMessageWithContent(JSONNode jsonNode);

        public abstract Type GetClassName();
    }
}
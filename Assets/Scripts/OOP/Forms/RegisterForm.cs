using System;
using System.Reflection;
using Connections;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

namespace OOP.Forms
{
    public class RegisterForm : BaseForm
    {
        [SerializeField]
        public InputField EmailField;
        public InputField UsernameField;
        public InputField PasswordField;
        public InputField ConfirmationPasswordField;

        public override void ClickSendRequestButton()
        {
            JsonObjectInString = new JSONObject(delegate(JSONObject request1)
            {
                request1.AddField("username", UsernameField.text);
                request1.AddField("email", EmailField.text);
                request1.AddField("password", PasswordField.text);
                request1.AddField("confirm_password", ConfirmationPasswordField.text);
            }).ToString();

            SendRequest(Endpoint.GetFullEndpoint(Endpoint.Register), "POST", this);
        }

        public override void FillSuccessMessageWithContent(JSONNode jsonNode)
        {
            string contentText = "Your have successfully created new account as " + jsonNode["data"]["username"] + ".";
            contentText += "To activate Your account go to email and fallow the steps.";
            PanelContent.text = contentText;
            IsSuccessResponse200 = true;
        }
        
        public override Type GetClassName()
        {
            return  MethodBase.GetCurrentMethod().DeclaringType;
        }
    }
}
using OOP.Config;

namespace OOP.Forms
{
    public class RequestForm
    {
        private string m_url;
        private string m_json;
        private string m_method;
        private string m_token;

        public RequestForm(string toUrl, string jsonBody, string methodType)
        {
            m_url = toUrl;
            m_json = jsonBody;
            m_method = methodType;
            m_token = "Bearer " + PlayerPrefsManager.GetToken();
        }

        public string MUrl
        {
            get { return m_url; }
            set { m_url = value; }
        }

        public string MJson
        {
            get { return m_json; }
            set { m_json = value; }
        }

        public string MMethod
        {
            get { return m_method; }
            set { m_method = value; }
        }

        public string MToken
        {
            get { return m_token; }
            set { m_token = value; }
        }
    }
}
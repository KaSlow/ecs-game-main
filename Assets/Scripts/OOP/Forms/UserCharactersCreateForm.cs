﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Connections;
using SimpleJSON;

namespace OOP.Forms
{
    public class UserCharactersCreateForm : BaseForm
    {
        [SerializeField] 
        public InputField NameField;

        public override void ClickSendRequestButton()
        {
            JsonObjectInString = new JSONObject(
                delegate(JSONObject request)
                {
                    request.AddField("name", NameField.text);
                }
            ).ToString();
            SendRequest(
                Endpoint.GetFullEndpoint(Endpoint.UserCharacters),
                "POST",
                this,
                true
            );
        }

        public override void FillSuccessMessageWithContent(JSONNode jsonNode)
        {
            string contentText = "New character has successfully created.";
            PanelContent.text = contentText;
            IsSuccessResponse200 = true;
        }

        public override Type GetClassName()
        {
            return MethodBase.GetCurrentMethod().DeclaringType;
        }
    }
}
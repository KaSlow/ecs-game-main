using System;
using System.Reflection;
using UnityEngine;
using Connections;
using SimpleJSON;
using UnityEngine.UI;

namespace OOP.Forms
{
    public class PasswordResetForm : BaseForm
    {
        [SerializeField]
        public InputField EmailField;

        public override void ClickSendRequestButton()
        {
            JsonObjectInString = new JSONObject(delegate(JSONObject request)
                {
                    request.AddField("email", EmailField.text);
                }
            ).ToString();
            SendRequest(Endpoint.GetFullEndpoint(Endpoint.PasswordReset), "POST", this);
        }

        public override void FillSuccessMessageWithContent(JSONNode jsonNode)
        {
            string contentText = "You have successfully reset Your password. " +
                                 "To complete changing it check Your e-mail box You will need it in next step.";
            PanelContent.text = contentText;
            IsSuccessResponse200 = true;
        }
        public override Type GetClassName()
        {
            return  MethodBase.GetCurrentMethod().DeclaringType;
        }
    }
}
﻿using System.Collections;
using Connections;
using OOP.Config;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace OOP.Forms
{
    public class UserCharactersForm : MonoBehaviour
    {
        private int m_ordinateX = -6;
        private int m_ordinateY = 50;
        [SerializeField] public Canvas Canvas;
        public Button ButtonPrefab;

        void Start()
        {
            StartCoroutine(LoadUserCharacters());
        }

        private IEnumerator LoadUserCharacters()
        {
            string url = Endpoint.GetFullEndpoint(Endpoint.UserCharacters);
            string method = "GET";

            RequestForm requestForm = new RequestForm(url, null, method);

            var request = new UnityWebRequest(requestForm.MUrl, requestForm.MMethod);
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", requestForm.MToken);

            yield return request.SendWebRequest();
            JSONNode jsonObject = JSON.Parse(request.downloadHandler.text);
            if (jsonObject.HasKey("data"))
            {
                int userCharactersNumber = jsonObject["data"].Count;
                for (int i = 0; i < userCharactersNumber; i++)
                {
                    string characterName = jsonObject["data"][i]["name"];
                    string characterId = jsonObject["data"][i]["id"];

                    Button newButton = CreateButton(characterName);
                    
                    m_ordinateY -= 30;
                    newButton.onClick.AddListener(
                        delegate
                        {
                            PlayerPrefsManager.SetCharacterId(characterId);
                            SceneManager.LoadScene(SceneLoader.MainScene);
                        }
                    );
                }

                if (userCharactersNumber < 5)
                {
                    Button newButton = CreateButton("Create new");
                    newButton.onClick.AddListener(
                        delegate
                        {
                            SceneManager.LoadScene(SceneLoader.UserCharacterCreateScene);
                        }
                    );
                }
            }
        }

        private Button CreateButton(string text)
        {
            Button newButton = Instantiate(
                ButtonPrefab,
                new Vector3(m_ordinateX, m_ordinateY),
                Quaternion.identity
            );
                    
            newButton.transform.SetParent(Canvas.transform, false);
            
            newButton.GetComponentInChildren<Text>().text = text;
            return newButton;
        }
    }
}
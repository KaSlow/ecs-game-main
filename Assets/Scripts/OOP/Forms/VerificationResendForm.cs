using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Connections;
using SimpleJSON;

namespace OOP.Forms
{
    public class VerificationResendForm : BaseForm
    {
        [SerializeField]
        public InputField EmailField;

        public override void ClickSendRequestButton()
        {
            JsonObjectInString = new JSONObject(delegate(JSONObject request)
                {
                    request.AddField("email", EmailField.text);
                }
            ).ToString();
            SendRequest(Endpoint.GetFullEndpoint(Endpoint.VerificationResend), "POST", this);
        }

        public override void FillSuccessMessageWithContent(JSONNode jsonNode)
        {
            string contentText = "Verification email has been resend on Your address.";
            PanelContent.text = contentText;
            IsSuccessResponse200 = true;
        }

        public override Type GetClassName()
        {
            return MethodBase.GetCurrentMethod().DeclaringType;
        }
    }
}
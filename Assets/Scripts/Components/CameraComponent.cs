﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Components
{
	public struct CameraComponent : IComponentData
	{
		public Vector3 Position;
	}
}

﻿using System;
using Unity.Entities;
using UnityEngine;

namespace Components
{
	[Serializable]
	public struct UVOffsetComponent : IComponentData
	{
		public Vector4 Value;

		public UVOffsetComponent(Vector4 Value)
		{
			this.Value = Value;
		}

		public UVOffsetComponent(float x, float y, float z, float w)
		{
			Value = new Vector4(x, y, z, w);
		}
	}
}
﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Components
{
	[Serializable]
	public struct GPUInstance : ISharedComponentData
	{
		public Texture2D sprite;
		public int pixelsPerUnit;
		public float2 pivot;
		public Material material;

		public GPUInstance(Texture2D sprite, int pixelsPerUnit, float2 pivot, Material sharedMaterial)
		{
			this.sprite = sprite;
			this.pixelsPerUnit = pixelsPerUnit;
			this.pivot = pivot;
			material = sharedMaterial;
		}
	}

	public class GPUInstancedComponent : SharedComponentDataProxy<GPUInstance>
	{ }
}

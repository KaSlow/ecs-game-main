﻿using Unity.Entities;

namespace Components
{
	public struct Enemy : IComponentData
	{
		public int CurrentRow;
		public int CurrentColumn;
		public EAnimationType AnimationType;
		public int Direction;
	}
}

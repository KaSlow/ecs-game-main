using UnityEngine;
using UnityEngine.UI;

namespace Components
{
    public class ButtonPrefabComponent : MonoBehaviour
    {
        [SerializeField] 
        public Text Text;
        public Button Button;
    }
}
using Unity.Entities;

namespace Components
{
	public struct StaminaComponent : IComponentData
	{
		public float CurrentStamina;
		public float MaxStamina;
		public float ReplenishRate;
	}
}

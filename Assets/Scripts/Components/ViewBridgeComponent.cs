﻿using Unity.Entities;

namespace Components
{
	public struct ViewBridgeComponent : IComponentData
	{
		public int Index;

		public ViewBridgeComponent(int index)
		{
			Index = index;
		}
	}
}

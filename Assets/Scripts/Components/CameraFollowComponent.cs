﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
	public struct CameraFollowComponent : IComponentData
	{
		public float3 Position;
	}
}

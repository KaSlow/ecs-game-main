﻿using Unity.Entities;

namespace Components
{
	public struct PlayerAttackComponent : IComponentData
	{
		public bool Attack;
	}
}

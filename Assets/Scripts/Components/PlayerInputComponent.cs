﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
	public struct PlayerInputComponent : IComponentData
	{
		public float3 Move;
	}
}

﻿using Unity.Entities;

namespace Components
{
	public struct PlayerDodgeComponent : IComponentData
	{
		public int Dodge;
	}
}

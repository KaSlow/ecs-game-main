﻿public interface IPooledObject : IUnityObject
{
	#region PublicFields

	bool Free { get; set; }

	#endregion

	#region PublicMethods

	void Begin();
	void Execute();
	void End();

	#endregion
}

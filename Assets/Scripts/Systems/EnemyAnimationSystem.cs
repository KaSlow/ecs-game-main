﻿using Components;
using Unity.Entities;
using UnityEngine;

public class EnemyAnimationSystem : ComponentSystem
{
	#region Statics

	private static readonly float FramesPerSecond = 60f;
	private static float m_cooldown;
	private static readonly int Columns = 7;
	private static readonly int Rows = 7;
	private static int m_currentRow;
	private static int m_currentColumn;
	private static Vector2 m_textureSize;
	private static bool m_initialized;

	#endregion

	#region PrivateFields

	[Inject] private Data m_data;

	#endregion

	#region PublicMethods

	public static void Initialize()
	{
		m_cooldown = CalculateCooldown();
		m_textureSize = new Vector2(1f / Columns, 1f / Rows);
	}

	#endregion

	#region ProtectedMethods

	protected override void OnUpdate()
	{
		if (!m_initialized)
		{
			CalculateStartOffset();
			return;
		}

		m_cooldown -= Time.deltaTime;
		bool spawn = m_cooldown <= 0.0f;

		if (spawn)
		{
			m_cooldown = CalculateCooldown();

			for (int i = 0; i < m_data.Length; i++)
			{
				Enemy enemy;

				int currentRow = m_data.Enemy[i].CurrentRow;
				int currentColumn = m_data.Enemy[i].CurrentColumn;

				Vector2 offset = Vector2.zero;
				float y = (float)currentRow / Rows;
				float x = (float)currentColumn / Columns;

				offset.Set(x, y);

				PostUpdateCommands.SetComponent(
					m_data.SpawnedEntities[i],
					new UVOffsetComponent
					{
						Value = new Vector4(m_textureSize.x, m_textureSize.y, offset.x, offset.y)
					});

				if (currentColumn >= Columns - 1)
				{
					currentRow--;
					currentColumn = 0;
				}
				else
				{
					currentColumn++;
				}

				if (currentRow < 0)
				{
					currentRow = Rows - 1;
					currentColumn = 0;
				}
				enemy.CurrentRow = currentRow;
				enemy.CurrentColumn = currentColumn;
				enemy.Direction = m_data.Enemy[i].Direction;
				enemy.AnimationType = m_data.Enemy[i].AnimationType;
				
				m_data.Enemy[i] = enemy;
			}
		}
	}

	#endregion

	#region PrivateMethods

	private void CalculateStartOffset()
	{
		for (int i = 0; i < m_data.Length; i++)
		{
			Enemy enemy;

			enemy.CurrentRow = Random.Range(0, Rows - 1);
			enemy.CurrentColumn = Random.Range(0, Columns - 1);
			enemy.Direction = Random.Range(0, 7);
			enemy.AnimationType = (EAnimationType)Random.Range(0, 2);

			m_data.Enemy[i] = enemy;
		}

		m_initialized = true;
	}

	private static float CalculateCooldown()
	{
		return 1 / FramesPerSecond;
	}

	#endregion

	#region NestedTypes

	private struct Data
	{
		public readonly int Length;
		public EntityArray SpawnedEntities;
		public ComponentDataArray<Enemy> Enemy;
		public ComponentDataArray<UVOffsetComponent> UVOffset;
	}

	#endregion
}

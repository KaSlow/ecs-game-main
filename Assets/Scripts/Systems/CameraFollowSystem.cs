﻿using Components;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
	public class CameraFollowSystem : ComponentSystem
	{
		#region PrivateFields

		[Inject] private CameraData m_cameras;
		[Inject] private CameraFollowedObject m_followedObject;

		#endregion

		#region ProtectedMethods

		protected override void OnUpdate()
		{
			for (int i = 0; i < m_cameras.Length; ++i)
			{
				UpdateCameraPosition(m_followedObject.Follow[i].Position, i);
			}
		}

		#endregion

		private void UpdateCameraPosition(Vector3 cameraPosition, int index)
		{
			Vector3 newCameraPosition = new Vector3(cameraPosition.x, cameraPosition.y, cameraPosition.z - Bootstrap.Settings.GeneralSettings.CameraOffset);
			Events.NotifyOnUnitPositionChanged(m_cameras.View[index].Index, newCameraPosition);
		}
		
		#region NestedTypes

		private struct CameraData
		{
			public readonly int Length;
			public ComponentDataArray<CameraComponent> Camera;
			public ComponentDataArray<ViewBridgeComponent> View;
		}		
		
		private struct CameraFollowedObject
		{
			public readonly int Length;
			public ComponentDataArray<CameraFollowComponent> Follow;
		}

		#endregion
	}
}

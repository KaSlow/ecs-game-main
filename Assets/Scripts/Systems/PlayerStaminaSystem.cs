using Components;
using Unity.Entities;
using UnityEngine;

namespace Systems
{
	public class PlayerStaminaSystem: ComponentSystem
	{
		[Inject] private UICanvasData m_canvas;
		[Inject] private StaminaData m_stamina;
		
		protected override void OnUpdate()
		{
			for (int i = 0; i < m_canvas.Length; ++i)
			{
				StaminaComponent staminaComponent = m_stamina.Stamina[i];
				staminaComponent.CurrentStamina += Mathf.Clamp(Time.deltaTime * staminaComponent.ReplenishRate, 0.0f, staminaComponent.MaxStamina);
				UpdateStaminaState(staminaComponent.CurrentStamina, staminaComponent.MaxStamina, i);
			}
		}

		private void UpdateStaminaState(float currentStamina, float maxStamina, int index)
		{
			Events.NotifyOnPlayerStaminaChanged(m_canvas.View[index].Index, currentStamina, maxStamina);
		}
		
		private struct UICanvasData
		{
			public readonly int Length;
			public ComponentDataArray<UICanvasComponent> UICanvas;
			public ComponentDataArray<ViewBridgeComponent> View;
		}			
		
		private struct StaminaData
		{
			public readonly int Length;
			public ComponentDataArray<StaminaComponent> Stamina;
			public ComponentDataArray<PlayerComponent> Player;
			
		}		
	}
}

﻿using System.Collections.Generic;
using Components;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.Rendering;
using VRD.Core.Assemblies;

namespace Systems
{
	[UpdateAfter(typeof(PreLateUpdate.ParticleSystemBeginUpdateAll))]
	[ExecuteInEditMode]
	public class GPUInstancingSystem : ComponentSystem
	{
		#region PrivateFields

		private readonly Dictionary<GPUInstance, Material> materialCache = new Dictionary<GPUInstance, Material>();
		private readonly Dictionary<GPUInstance, Mesh> meshCache = new Dictionary<GPUInstance, Mesh>();

		private readonly Matrix4x4[] matricesArray = new Matrix4x4[1023];
		private readonly Vector4[] offsetArray = new Vector4[1023];
		private readonly List<GPUInstance> cacheduniqueRendererTypes = new List<GPUInstance>(10);
		private ComponentGroup instanceRendererGroup;
		private MaterialPropertyBlock m_materialProperty;
		#endregion

		#region ProtectedMethods

		protected override void OnStartRunning()
		{
			instanceRendererGroup = GetComponentGroup(typeof(GPUInstance), typeof(LocalToWorld), typeof(UVOffsetComponent), typeof(Enemy));
			m_materialProperty = new MaterialPropertyBlock();
		}

		protected override void OnUpdate()
		{
			EntityManager.GetAllUniqueSharedComponentData(cacheduniqueRendererTypes);
			for (int i = 0; i != cacheduniqueRendererTypes.Count; i++)
			{
				GPUInstance renderer = cacheduniqueRendererTypes[i];
				if (renderer.sprite == null)
				{
					continue;
				}

				instanceRendererGroup.SetFilter(renderer);
				ComponentDataArray<LocalToWorld> transforms = instanceRendererGroup.GetComponentDataArray<LocalToWorld>();
				ComponentDataArray<UVOffsetComponent> uvOffsets = instanceRendererGroup.GetComponentDataArray<UVOffsetComponent>();
				ComponentDataArray<Enemy> enemies = instanceRendererGroup.GetComponentDataArray<Enemy>();

				Mesh mesh;
				Material material;
				
				float size = math.max(renderer.sprite.width, renderer.sprite.height) / (float)renderer.pixelsPerUnit;
				float2 meshPivot = renderer.pivot * size;
				if (!meshCache.TryGetValue(renderer, out mesh))
				{
					mesh = MeshUtils.GenerateQuad(size, meshPivot);
					meshCache.Add(renderer, mesh);
				}

				if (!materialCache.TryGetValue(renderer, out material))
				{
					material = new Material(renderer.material)
					{
						enableInstancing = true,
						mainTexture = renderer.sprite
					};
					materialCache.Add(renderer, material);
				}

				int beginIndex = 0;
				while (beginIndex < transforms.Length)
				{
					int length = math.min(matricesArray.Length, transforms.Length - beginIndex);
					CopyMatrices(transforms, beginIndex, length, matricesArray);
					int k = 0;

					for (int j = beginIndex; j < beginIndex + length; ++j)
					{
						Vector4 offset = uvOffsets[j].Value;
						offsetArray[k++] = new Vector4(offset.x, offset.y, offset.z, offset.w);
					}

					m_materialProperty = new MaterialPropertyBlock();
					m_materialProperty.SetVectorArray("_MainTex_ST", offsetArray);

					Graphics.DrawMeshInstanced(mesh, 0, material, matricesArray, length, 
						m_materialProperty, ShadowCastingMode.Off, false);
					
					beginIndex += length;
				}
			}

			cacheduniqueRendererTypes.Clear();
		}

		#endregion

		#region PrivateMethods

		private static unsafe void CopyMatrices(ComponentDataArray<LocalToWorld> transforms, int beginIndex, int length, Matrix4x4[] outMatrices)
		{
			fixed (Matrix4x4* matricesPtr = outMatrices)
			{
				Assert.AreEqual(sizeof(Matrix4x4), sizeof(LocalToWorld));
				NativeSlice<LocalToWorld> matricesSlice = NativeSliceUnsafeUtility.ConvertExistingDataToNativeSlice<LocalToWorld>(matricesPtr, sizeof(Matrix4x4), length);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				NativeSliceUnsafeUtility.SetAtomicSafetyHandle(ref matricesSlice, AtomicSafetyHandle.GetTempUnsafePtrSliceHandle());
#endif
				transforms.CopyTo(matricesSlice, beginIndex);
			}
		}

		#endregion
	}
}

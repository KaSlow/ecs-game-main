﻿using Components;
using Unity.Entities;
using UnityEngine;

namespace Systems
{
	public class PlayerInputSystem : ComponentSystem
	{
		#region PrivateFields

		[Inject] private PlayerData m_players;

		#endregion

		#region ProtectedMethods

		protected override void OnUpdate()
		{
			float dt = Time.deltaTime;

			for (int i = 0; i < m_players.Length; ++i)
			{
				UpdatePlayerInput(i, dt);
			}
		}

		#endregion

		#region PrivateMethods

		private void UpdatePlayerInput(int i, float dt)
		{
			PlayerInputComponent pi;
			PlayerDodgeComponent dodge;
			
			dodge.Dodge = 0;
			pi.Move.x = Input.GetAxis("Horizontal");
			pi.Move.y = Input.GetAxis("Vertical");
			pi.Move.z = 0.0f;

			if (Input.GetKeyDown(KeyCode.D))
			{
				dodge.Dodge = 1;
			}

			m_players.Input[i] = pi;
			m_players.Dodge[i] = dodge;
		}

		#endregion

		#region NestedTypes

		private struct PlayerData
		{
			public readonly int Length;
			public ComponentDataArray<PlayerInputComponent> Input;
			public ComponentDataArray<PlayerDodgeComponent> Dodge;
		}

		#endregion
	}
}

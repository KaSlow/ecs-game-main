﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Systems
{
	internal class EnemySpawnSystem : ComponentSystem
	{
		private bool m_spawned;
		
		#region PublicMethods
		public struct Data
		{
			public readonly int Length;
			public ComponentDataArray<Position> Position;
			public ComponentDataArray<Rotation> Heading;
		}

		[Inject] private Data m_Data;

		public static void SetupComponentData(EntityManager entityManager)
		{
		}

		#endregion

		#region ProtectedMethods

		protected override void OnUpdate()
		{
			if (!m_spawned)
			{
				for (int i = 0; i < Bootstrap.Settings.InstancingSettings.enemiesAmount; i++)
				{
					SpawnEnemy();
				}
				m_spawned = true;
			}
		}

		#endregion
		#region PrivateMethods

		private void SpawnEnemy()
		{
			PostUpdateCommands.CreateEntity(Bootstrap.EnemyArchetype);
			PostUpdateCommands.SetComponent(new Position { Value = new float3(Random.Range(-1.0f,1.0f) * 500, Random.Range(-1.0f,1.0f) * 500, 0)});
			// PostUpdateCommands.SetComponent(
			// 	new Rotation
			// 	{
			// 		Value =  quaternion.Euler(0, 0, Random.value * Mathf.PI * 2)
			// 	});

			PostUpdateCommands.AddSharedComponent(Bootstrap.GetLookFromPrototype(EAnimationType.Run, Random.Range(0, 7)));
		}

		#endregion
	}
}

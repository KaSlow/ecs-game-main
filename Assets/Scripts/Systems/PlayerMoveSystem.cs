﻿using Components;
using Settings;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
	public class PlayerMoveSystem : ComponentSystem
	{
		#region PrivateFields

		[Inject] private Data m_data;
		private float m_dodgeProgression;
		private float3 m_lastMoveVector;

		#endregion

		#region ProtectedMethods

		protected override void OnStartRunning()
		{
			base.OnStartRunning();
			m_dodgeProgression = 1.0f;
		}

		protected override void OnUpdate()
		{
			GameSettings settings = Bootstrap.Settings;

			float dt = Time.deltaTime;
			for (int index = 0; index < m_data.Length; ++index)
			{
				float3 position = m_data.Position[index].Value;
				quaternion rotation = m_data.Heading[index].Value;

				PlayerInputComponent playerInput = m_data.Input[index];
				PlayerDodgeComponent playerDodge = m_data.Dodge[index];
				CameraFollowComponent cameraFollow = m_data.CameraFollow[index];

				if (m_dodgeProgression >= 1.0f)
				{
					if (playerDodge.Dodge == 1)
					{
						m_dodgeProgression = 0.0f;
						Events.NotifyOnDodge(m_data.View[index].Index, true, 0);
						m_lastMoveVector = Vector3.Normalize(playerInput.Move);					
					}
					else
					{
						position += dt * playerInput.Move * settings.PlayerSettings.PlayerMoveSpeed;
						playerDodge.Dodge = 0;
						Events.NotifyOnDodge(m_data.View[index].Index, false, 1);
					}
				}
				else
				{
					m_dodgeProgression += dt * settings.PlayerSettings.PlayerDodgeSpeed;
					Events.NotifyOnDodge(m_data.View[index].Index, true, m_dodgeProgression);
					Vector3 target = position + (m_lastMoveVector * settings.PlayerSettings.PlayerDodgeDistance);
					position = Vector3.Lerp(position, target, m_dodgeProgression);
				}

				var newPosition = new Position { Value = position };
				var newRotation = new Rotation { Value = rotation };
				cameraFollow.Position = position;
				
				m_data.Position[index] = newPosition;
				m_data.Heading[index] = newRotation;

				m_data.Input[index] = playerInput;
				m_data.Dodge[index] = playerDodge;
				m_data.CameraFollow[index] = cameraFollow;
				
				Events.NotifyOnUnitPositionChanged(m_data.View[index].Index, newPosition.Value);
				Events.NotifyOnUnitRotationChanged(m_data.View[index].Index, newRotation.Value);

				Events.NotifyOnInputChanged(m_data.View[index].Index, new Vector2(playerInput.Move.x, playerInput.Move.y));
			}
		}

		#endregion

		#region NestedTypes

		public struct Data
		{
			public readonly int Length;
			public ComponentDataArray<Position> Position;
			public ComponentDataArray<Rotation> Heading;
			public ComponentDataArray<PlayerInputComponent> Input;
			public ComponentDataArray<PlayerDodgeComponent> Dodge;
			public ComponentDataArray<CameraFollowComponent> CameraFollow;
			public ComponentDataArray<ViewBridgeComponent> View;
		}

		#endregion
	}
}

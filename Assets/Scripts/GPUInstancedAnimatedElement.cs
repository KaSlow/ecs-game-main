using System;
using System.Collections.Generic;
using Components;

[Serializable]
public struct GPUInstancedAnimatedElement
{
	public EAnimationType AnimationType;
	public List<GPUInstance> GPUAnimationInstances;
}
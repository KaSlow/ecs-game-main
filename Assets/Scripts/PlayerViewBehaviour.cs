﻿using System;
using UnityEngine;

public class PlayerViewBehaviour : BehaviourBase
{
	#region Constants

	private const string DIR_X = "DirX";
	private const string DIR_Y = "DirY";
	private const string LAST_DIR_X = "LastDirX";
	private const string LAST_DIR_Y = "LastDirY";
	private const string MOVEMENT = "Movement";
	private const string DODGE = "Dodge";
	private const string DODGEPROGRESSION = "DodgeProgression";

	#endregion

	#region Statics

	private static readonly int DirX = Animator.StringToHash(DIR_X);
	private static readonly int DirY = Animator.StringToHash(DIR_Y);
	private static readonly int LastDirX = Animator.StringToHash(LAST_DIR_X);
	private static readonly int LastDirY = Animator.StringToHash(LAST_DIR_Y);
	private static readonly int Movement = Animator.StringToHash(MOVEMENT);
	private static readonly int DodgeHash = Animator.StringToHash(DODGE);
	private static readonly int DodgeProgressionHash = Animator.StringToHash(DODGEPROGRESSION);

	#endregion

	#region PublicFields

	public override EUnitType Type => EUnitType.Player;

	#endregion

	#region SerializeFields

	[SerializeField] private Animator m_animator;

	#endregion

	#region PrivateFields

	private float m_lastX;
	private float m_lastY;

	#endregion

	#region ProtectedMethods

	protected override void Move()
	{
		base.Move();

		transform.position = Position;
	}

	protected override void Rotate()
	{
		base.Rotate();

		transform.rotation = Rotation;
	}

	protected override void UpdateInput()
	{
		base.UpdateInput();

		if (Dodge)
		{
			if (!m_animator.GetBool(DodgeHash))
			{
				m_animator.SetBool(DodgeHash, true);
				m_animator.SetFloat(LastDirX, Heading.x);
				m_animator.SetFloat(LastDirY, Heading.y);
			}
			m_animator.SetFloat(DodgeProgressionHash, DodgeProgress);
		}
		else
		{
			if (Math.Abs(Heading.x) <= 0 && Math.Abs(Heading.y) <= 0)
			{
				m_animator.SetFloat(LastDirX, m_lastX);
				m_animator.SetFloat(LastDirY, m_lastY);

				m_animator.SetBool(Movement, false);
			}
			else
			{
				m_lastX = Heading.x;
				m_lastY = Heading.y;

				m_animator.SetBool(Movement, true);
			}

			m_animator.SetFloat(DirX, Heading.x);
			m_animator.SetFloat(DirY, Heading.y);
			m_animator.SetBool(DodgeHash, false);
		}
	}

	#endregion
}

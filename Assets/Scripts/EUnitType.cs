﻿public enum EUnitType
{
	None,
	Player,
	Enemy,
	Camera,
	UICanvas,
}

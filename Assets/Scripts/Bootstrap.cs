﻿using System.Collections.Generic;
using System.Linq;
using Systems;
using Components;
using Settings;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Bootstrap
{
	#region Statics

	public static GameSettings Settings;
	public static EntityArchetype EnemyArchetype;
	public static List<GPUInstancedAnimatedElement> EnemyGPUInstances;

	private static EntityArchetype m_playerArchetype;
	private static EntityArchetype m_cameraArchetype;
	private static EntityArchetype m_uiArchetype;
	private static Player m_player;
	private static Camera m_camera;
	private static UI m_uiCanvas;
	private static ViewFactory m_viewFactory;

	#endregion

	#region PublicMethods

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	public static void Initialize()
	{
		var entityManager = World.Active.GetOrCreateManager<EntityManager>();

		m_viewFactory = new ViewFactory();

		m_playerArchetype = entityManager.CreateArchetype(
			typeof(PlayerComponent),
			typeof(Position),
			typeof(Rotation),
			typeof(PlayerInputComponent),
			typeof(PlayerDodgeComponent),
			typeof(CameraFollowComponent),
			typeof(ViewBridgeComponent));

		EnemyArchetype = entityManager.CreateArchetype(
			typeof(Enemy),
			typeof(Position),
			typeof(Rotation),
			typeof(UVOffsetComponent));

		m_cameraArchetype = entityManager.CreateArchetype(
			typeof(CameraComponent),
			typeof(Position),
			typeof(Rotation),
			typeof(ViewBridgeComponent));

		m_uiArchetype = entityManager.CreateArchetype(
			typeof(UICanvasComponent),
			typeof(ViewBridgeComponent));
	}

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
	public static void InitializeAfterSceneLoad()
	{
		GameObject gameSettings = GameObject.Find("GameSettings");
		if (gameSettings == null)
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
			return;
		}

		InitializeWithScene();
	}

	public static GPUInstance GetLookFromPrototype(EAnimationType animType, int direction)
	{
		var gpuInstance = new GPUInstance();

		if (Settings != null)
		{
			List<GPUInstancedAnimatedElement> animList = Settings.InstancingSettings.EnemyInstancing.enemyInstances;

			if (animList.Exists(c => c.AnimationType == animType))
			{
				List<GPUInstance> gpuList = animList.FirstOrDefault(i => i.AnimationType == animType).GPUAnimationInstances;

				if (gpuList.Count >= direction)
				{
					gpuInstance = gpuList[direction];
				}
				else
				{
					Debug.Log($"There is no GPUInstance Settings for animation of type: {animType} for {direction} direction");
				}
			}
			else
			{
				Debug.Log($"There is no GPUInstance Settings for animation of type: {animType}");
			}
		}

		return gpuInstance;
	}

	#endregion

	#region PrivateMethods

	private static void InitializeWithScene()
	{
		GameObject gameSettings = GameObject.Find("GameSettings");
		if (gameSettings == null)
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
			return;
		}

		Settings = gameSettings.GetComponent<GameSettings>();

		if (Settings == null)
		{
			return;
		}

		var entityManager = World.Active.GetOrCreateManager<EntityManager>();
		EnemySpawnSystem.SetupComponentData(entityManager);
		EnemyAnimationSystem.Initialize();

		CreateCamera();
		CreatePlayer();
	}

	private static void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
	{
		InitializeWithScene();
	}

	private static void CreatePlayer()
	{
		var entityManager = World.Active.GetOrCreateManager<EntityManager>();

		Entity player = entityManager.CreateEntity(m_playerArchetype);

		m_player = new Player();

		entityManager.SetComponentData(player, new Position { Value = new float3(0.0f, 0.0f, 0.0f) });
		entityManager.SetComponentData(player, new Rotation { Value = Quaternion.identity });

		SetViewBridge(player, EGameViewType.PlayerView, ref m_player.ViewID);
	}

	private static void CreateCamera()
	{
		var entityManager = World.Active.GetOrCreateManager<EntityManager>();

		Entity camera = entityManager.CreateEntity(m_cameraArchetype);

		m_camera = new Camera();

		SetViewBridge(camera, EGameViewType.Camera, ref m_camera.ViewID);
	}

	private static void CreateUI()
	{
		var entityManager = World.Active.GetOrCreateManager<EntityManager>();

		Entity uiCanvas = entityManager.CreateEntity(m_uiArchetype);

		m_uiCanvas = new UI();

		SetViewBridge(uiCanvas, EGameViewType.UICanvas, ref m_uiCanvas.ViewID);
	}

	private static void SetViewBridge(Entity entity, EGameViewType viewType, ref int existingViewId)
	{
		var entityManager = World.Active.GetOrCreateManager<EntityManager>();

		existingViewId = existingViewId == -1 ? Events.CreateUnit(viewType, m_viewFactory) : existingViewId;
		entityManager.SetComponentData(entity, new ViewBridgeComponent(existingViewId));
	}

	#endregion
}

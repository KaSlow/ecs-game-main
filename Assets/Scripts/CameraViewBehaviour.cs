﻿public class CameraViewBehaviour : BehaviourBase
{
	#region PublicFields

	public override EUnitType Type => EUnitType.Camera;

	#endregion

	#region ProtectedMethods

	protected override void Move()
	{
		base.Move();

		transform.position = Position;
	}

	protected override void Rotate()
	{
		base.Rotate();

		transform.rotation = Rotation;
	}

	#endregion
}

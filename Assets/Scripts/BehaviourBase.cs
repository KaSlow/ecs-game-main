﻿using UnityEngine;

public abstract class BehaviourBase : MonoBehaviour, IPooledObject
{
	#region PublicFields

	public Behaviour behaviour => this;
	public bool Free { get; set; }
	public abstract EUnitType Type { get; }

	public Vector3 Position { get; private set; }
	public Vector3 Heading { get; private set; }

	public Quaternion Rotation { get; protected set; }

	public bool Dodge { get; private set; }
	public float DodgeProgress { get; private set; }

	public float CurrentStamina { get; private set; }

	public float MaxStamina { get; private set; }

	#endregion

	#region UnityMethods

	private void Update()
	{
		Execute();
	}

	#endregion

	#region InterfaceImplementations

	public void Begin()
	{ }

	public void Execute()
	{ }

	public void End()
	{ }

	#endregion

	#region ProtectedMethods

	protected virtual void Move()
	{ }

	protected virtual void Rotate()
	{ }

	protected virtual void UpdateInput()
	{ }

	protected void UpdateStamina()
	{ }

	#endregion

	#region AllOtherMembers

	internal void OnPositionChange(Vector3 position)
	{
		Position = position;
		Move();
	}

	internal void OnRotationChange(Quaternion rotation)
	{
		Rotation = rotation;
		Rotate();
	}

	internal void OnInputUpdated(Vector3 heading)
	{
		Heading = heading;
		UpdateInput();
	}

	internal void OnDodgePressed(bool dodge, float progress)
	{
		Dodge = dodge;
		DodgeProgress = progress;
	}

	internal void OnStaminaChanged(float currentStamina, float maxStamina)
	{
		CurrentStamina = currentStamina;
		MaxStamina = maxStamina;
		UpdateStamina();
	}

	#endregion
}

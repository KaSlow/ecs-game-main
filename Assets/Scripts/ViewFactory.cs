using System;
using System.Collections.Generic;
using UnityEngine;
using VRD.Core.Assemblies;
using Object = UnityEngine.Object;

public class ViewFactory
{
	#region PrivateFields

	private readonly PrefabAssembly m_prefabAssembly;
	private readonly Dictionary<string, BehaviourBase> m_loadedPrefabs;
	
	#endregion

	#region Constructors

	public ViewFactory()
	{
		m_loadedPrefabs = new Dictionary<string, BehaviourBase>();
		m_prefabAssembly = new PrefabAssembly();
	}

	#endregion

	#region PublicMethods

	public T Create<T>(EGameViewType gameViewType)
		where T : BehaviourBase
	{
		string prefabPath = m_prefabAssembly.GetPrefab(gameViewType);

		if (prefabPath == string.Empty)
		{
			throw new TypeLoadException($"Can't find view({typeof(T)}) for: {gameViewType.ToString()}");
		}

		return CreateInstance<T>(prefabPath);
	}

	#endregion

	#region PrivateMethods

	private T CreateInstance<T>(string path)
		where T : BehaviourBase
	{
		T view = Object.Instantiate(GetPrefab<T>(path));
		T targetView = view;

		if (targetView == null)
		{
			Object.Destroy(view);
			throw new TypeLoadException($"Failed to instantiate view({typeof(T)}: {path}");
		}

		return targetView;
	}
	
	private T GetPrefab<T>(string path)
		where T : BehaviourBase
	{
		BehaviourBase prefab;
		return (T)(m_loadedPrefabs.TryGetValue(path, out prefab) ? prefab : LoadPrefab<T>(path));
	}

	private T LoadPrefab<T>(string path)
		where T : BehaviourBase
	{
		var prefab = Resources.Load<T>(path);
		if (prefab == null)
		{
			throw new TypeLoadException($"Failed to load prefab({typeof(T)}): {path}");
		}

		m_loadedPrefabs.Add(path, prefab);
		return prefab;
	}

	#endregion
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VRD.Core.Assemblies
{
	public sealed class EnemySpriteSheetAssembly
	{
		#region PrivateFields

		private readonly Dictionary<EAnimationType, Texture2D[]> m_textures;

		#endregion

		#region Constructors

		public EnemySpriteSheetAssembly(List<Texture2D[]> textures)
		{
			m_textures = new Dictionary<EAnimationType, Texture2D[]>
			{
				[EAnimationType.Idle] = textures[0],
				[EAnimationType.Run] = textures[1],
				[EAnimationType.Attack] = textures[2],
			};
		}

		#endregion

		#region PublicMethods

		public Texture2D[] GetAnimationTypeTextureArray(EAnimationType animationType)
		{
			if (!m_textures.ContainsKey(animationType))
			{
				throw new TypeLoadException(string.Format("Can't find textures for animation type: {0}", animationType));
			}

			return m_textures[animationType];
		}

		#endregion
	}
}

using Connections.Configs;

namespace Connections
{
    public static class Endpoint
    {
        public const string Register = "register";
        public const string Login = "login";
        public const string UserCharacters = "user/characters";
        public const string PasswordChange = "password/change";
        public const string PasswordReset = "password/reset";
        public const string VerificationResend = "verification/resend";
     
        // With argument - character.id
        public const string Game = "game";

        /// <returns>
        /// Full route to specified endpoint.
        /// </returns>
        public static string GetMainRoute()
        {
            return Api.Host + "/" + Api.Prefix + "/";
        }

        /// <example>
        /// This simple shows how to use it
        /// <code>
        /// GetFullEndpoint(Register)
        /// </code>
        /// </example>
        /// <returns>
        /// Full route to specified endpoint.
        /// </returns>
        public static string GetFullEndpoint(string endpointName, string endpointArgument = "")
        {
            string argument = endpointArgument != "" ? "/" + endpointArgument : "";
            return GetMainRoute() + endpointName + argument;
        }
    }
}
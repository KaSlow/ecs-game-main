namespace Connections.Configs
{
    public abstract class Api
    {
        /// <summary>
        /// Local server
        /// </summary>
        public const string Host = "localhost:8000";
      
//        /// <summary>
//        /// Hosting server
//        /// </summary>
  //      public const string Host = "http://dev.rpg.zachariasz.usermd.net/";
        public const string Prefix = "api";
    }
}
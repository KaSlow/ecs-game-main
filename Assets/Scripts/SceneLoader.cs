﻿using UnityEngine;
using UnityEngine.SceneManagement;
 
public class SceneLoader : MonoBehaviour
{
    public const string MainScene = "MainScene";
    public const string UserCharacterCreateScene = "UserCharacterCreateScene";

    public void LoadSceneOnClick(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
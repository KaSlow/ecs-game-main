﻿using System.Collections.Generic;
using UnityEngine;

public sealed class Events
{
	#region Statics

	public static bool Disable = false;
	public static readonly Dictionary<int, BehaviourBase> Behaviours = new Dictionary<int, BehaviourBase>();
	private static int m_lastId;

	#endregion

	#region PublicMethods

	public static BehaviourBase GetViewReference(int index)
	{
		BehaviourBase behaviour = Behaviours[index];
		return behaviour;
	}

	#endregion

	#region PrivateMethods

	private static bool CanExecute(int index, out BehaviourBase behaviour)
	{
		behaviour = null;

		behaviour = Behaviours[index];

		return behaviour != null;
	}

	private static int GetNextID()
	{
		return ++m_lastId;
	}

	#endregion

	#region AllOtherMembers

	internal static void NotifyOnPlayerStaminaChanged(int index, float currentStamina, float maxStamina)
	{
		BehaviourBase behaviour = Behaviours[index];

		if (behaviour != null)
		{
			behaviour.OnStaminaChanged(currentStamina, maxStamina);
		}
	}

	internal static void NotifyOnUnitPositionChanged(int index, Vector3 position)
	{
		BehaviourBase behaviour = Behaviours[index];

		if (behaviour != null)
		{
			behaviour.OnPositionChange(position);
		}
	}

	internal static void NotifyOnUnitRotationChanged(int index, Quaternion rotation)
	{
		BehaviourBase behaviour = Behaviours[index];

		if (behaviour != null)
		{
			behaviour.OnRotationChange(rotation);
		}
	}

	internal static void NotifyOnInputChanged(int index, Vector3 heading)
	{
		BehaviourBase behaviour = Behaviours[index];

		if (behaviour != null)
		{
			behaviour.OnInputUpdated(heading);
		}
	}

	internal static void NotifyOnDodge(int index, bool dodge, float progress)
	{
		BehaviourBase behaviour = Behaviours[index];

		if (behaviour != null)
		{
			behaviour.OnDodgePressed(dodge, progress);
		}
	}

	internal static int CreateUnit(EGameViewType type, ViewFactory viewFactory)
	{
		var obj = viewFactory.Create<BehaviourBase>(type);

		int index = GetNextID();
		Behaviours.Add(index, obj.behaviour as BehaviourBase);
		obj.Begin();

		return index;
	}

	#endregion
}

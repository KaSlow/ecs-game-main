﻿using UnityEngine;

namespace Settings
{
	[CreateAssetMenu(fileName = "PlayerSettings", menuName = "Settings/Player", order = 1)]
	public class PlayerSettings : ScriptableObject
	{
		#region PublicFields

		public float PlayerMoveSpeed;
		public float PlayerDodgeSpeed;
		public float PlayerDodgeDistance;

		#endregion
	}
}

﻿using System.Collections.Generic;
using Components;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyInstancingSettings", menuName = "Settings/EnemyInstancing", order = 2)]
public class EnemyInstancingSettings : ScriptableObject
{
	public List<GPUInstancedAnimatedElement> enemyInstances;
}

﻿using UnityEngine;

namespace Settings
{
	[CreateAssetMenu(fileName = "GeneralSettings", menuName = "Settings/General", order = 1)]
	public class GeneralSettings : ScriptableObject
	{
		#region PublicFields

		public float CameraOffset;

		#endregion
	}
}

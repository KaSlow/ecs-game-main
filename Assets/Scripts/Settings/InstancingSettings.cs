﻿using Unity.Mathematics;
using UnityEngine;

namespace Settings
{
	[CreateAssetMenu(fileName = "InstancingSettings", menuName = "Settings/Instancing", order = 2)]
	public class InstancingSettings : ScriptableObject
	{
		#region PublicFields

		public EnemyInstancingSettings EnemyInstancing;
		public int enemiesAmount;

		#endregion
	}
}

﻿using UnityEngine;

namespace Settings
{
	public class GameSettings : MonoBehaviour
	{
		#region PublicFields

		public GeneralSettings GeneralSettings;
		public PlayerSettings PlayerSettings;
		public InstancingSettings InstancingSettings;

		#endregion
	}
}

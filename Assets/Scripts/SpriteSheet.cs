﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSheet : MonoBehaviour
{
	[SerializeField] private MeshRenderer _myRenderer;
	public int Columns = 5;
	public int Rows = 5;
	public float FramesPerSecond = 10f;
	public bool RunOnce = true;
	private Material materialCopy = null;
	private static readonly int MainTex = Shader.PropertyToID("_MainTex");

	void Start()
	{
		_myRenderer = GetComponent<MeshRenderer>();
		materialCopy = new Material(_myRenderer.sharedMaterial);
		_myRenderer.sharedMaterial = materialCopy;

		Vector2 size = new Vector2(1f / Columns, 1f / Rows);
		_myRenderer.sharedMaterial.SetTextureScale(MainTex, size);
	}

	void OnEnable()
	{
		StartCoroutine(UpdateTiling());
	}

	private IEnumerator UpdateTiling()
	{
		float x = 0f;
		float y = 0f;
		Vector2 offset = Vector2.zero;

		while (true)
		{
			for (int i = Rows - 1; i >= 0; i--) // y
			{
				y = (float)i / Rows;

				for (int j = 0; j <= Columns - 1; j++) // x
				{
					x = (float)j / Columns;

					offset.Set(x, y);

					_myRenderer.sharedMaterial.SetTextureOffset(MainTex, offset);
					yield return new WaitForSeconds(1f / FramesPerSecond);
				}
			}

			if (RunOnce)
			{
				yield break;
			}
		}
	}
}

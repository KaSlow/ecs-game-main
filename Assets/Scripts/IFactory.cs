﻿using System.Collections.Generic;

public interface IFactory<TEnum>
{
	#region PublicFields

	Dictionary<TEnum, IPooledObject> Prefabs { get; }
	Dictionary<TEnum, List<IPooledObject>> Pools { get; }

	#endregion

	#region PublicMethods

	IPooledObject Get(TEnum type);

	#endregion
}

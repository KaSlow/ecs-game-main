using System.Collections.Generic;

namespace VRD.Core.Assemblies
{
	public class PrefabAssembly
	{
		#region PrivateFields

		private readonly Dictionary<EGameViewType, string> m_prefabs;

		#endregion

		#region Constructors

		public PrefabAssembly()
		{
			m_prefabs = new Dictionary<EGameViewType, string>();
			
			//Here we keep prefab paths, prefabs has to be in Resources folder to be instantiated
			m_prefabs[EGameViewType.PlayerView] = "Players/Player";
			m_prefabs[EGameViewType.Camera] = "SceneElements/MainCamera";
			m_prefabs[EGameViewType.UICanvas] = "UI/UICanvas";
		}

		#endregion

		#region InterfaceImplementations

		public string GetPrefab(EGameViewType gameViewType)
		{
			return !m_prefabs.ContainsKey(gameViewType) ? string.Empty : m_prefabs[gameViewType];
		}

		#endregion
	}
}
